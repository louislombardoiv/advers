# Project 2

PlayerO and PlayerX are identical. They implement the same alpha beta pruning algo.

_There is no database file._

Moves are generated at runtime. The algorithm has two parameters:

```
MAX_DURATION = 3.0
MAX_DEPTH = 2
```

`MAX_DEPTH` is how deep the tree should go on any particular node.
`MAX_DURATION` is how long the script has to calculate it's next move.

The output of the algo. is given by a yield generator so stopping it while it's
running doesn't cause any issues. Setting a `MAX_DURATION` lower than 2.0 though
will not likely produce usable results.

## Running

The sample tournament code works fine here.

```
python3 Tournament.py
```

## Extras

There are some extra files included here just for fun.

BoardMaker.py is a script I wrote for viewing / creating the board as a bit
string. This way I could create tests visually rather than having to think
about the bit strings.

- Controls for moving around the cursor are WASD

- "X" marks a location as Player X
- "O" marks a location as Player O
- "-" marks a location as empty
