# b'0000000001000000100000010000001000000000000000000'
import PlayerX


def connected_four(position):
    # Horizontal check
    m = position & (position >> 7)
    if m & (m >> 14):
        return True

    # Diagonal \
    m = position & (position >> 6)
    if m & (m >> 12):
        return True

    # Diagonal /
    m = position & (position >> 8)
    if m & (m >> 16):
        return True

    # Vertical
    m = position & (position >> 1)
    if m & (m >> 2):
        return True

    # Nothing found
    return False


def connected_three(position):
    # Horizontal check
    m = position & (position >> 7)
    if m & (m >> 7):
        return True

    # Diagonal \
    m = position & (position >> 6)
    if m & (m >> 6):
        return True

    # Diagonal /
    m = position & (position >> 8)
    if m & (m >> 8):
        return True

    # Vertical
    m = position & (position >> 1)
    if m & (m >> 1):
        return True

    # Nothing found
    return False


def print_board(bit_str):
    game_str = format(bit_str, '049b')
    print(game_str)
    for y in range(7):
        print(y, ": ", end="")
        for x in reversed(range(7)):
            print(game_str[(x * 7) + y], end="")
        print("")


def print_full_board(bit_str, mask):
    position_str = format(bit_str, '049b')
    opp_str = format(bit_str ^ mask, '049b')

    print("---------")

    for y in range(7):
        print(y, ":", end="")
        for x in reversed(range(7)):
            if '1' == position_str[(x * 7) + y]:
                print('X', end="")
            elif '1' == opp_str[(x * 7) + y]:
                print('O', end="")
            else:
                print("-", end="")
        print("")


def make_move(position, mask, col):
    new_position = position ^ mask
    new_mask = mask | (mask + (1 << (col*7)))
    return new_position, new_mask


def simulate_moves(position, mask):
    for i in range(7):
        new_a, new_mask = make_move(position, mask, i)
        print_full_board(new_a ^ new_mask, new_mask)

        if connected_four(new_a ^ mask):
            print("Connected!")

        if connected_four(new_a):
            print("Lost!")

        if connected_three(new_a ^ mask):
            print("Three!")

        if connected_three(new_a):
            print("three!")


def bitstring_to_avail_moves(position, mask):
    # Return the avail. moves

    # Get a bit string rep. all the filled spaces
    occupied_places = position | mask
    position_str = format(occupied_places, '049b')

    # Check the bit string at specific points
    actions = [c for c in range(WIDTH) if position_str[HEIGHT * c] == '0']
    if actions:
        random.shuffle(actions)
    return actions


if __name__ == "__main__":
    a = int(b'0000000000000000000110001101000100000000000000000', 2)
    mask = int(b'0000000000000100000110011111000111100000000000000', 2)
    col = int(b'0000000000000000000000000000000000000000000111111', 2)

    row = int(b'0000001000000100000010000001000000100000010000001', 2)

    position = int(b'0000101000110100000000000001000000100000000000010', 2)
    mask = int(b'0001111001111100000000000001000111100000000000011', 2)

    testA = {
        "position": int(b'0000000000001100010100000011000000000000110000000', 2),
        "mask": int(b'0000001000011101111110000011000000100001110000000', 2),
        "solution": [2, 6]
    }

    for test in [testA]:
        position = test["position"]
        mask = test["mask"]
        solution = test["solution"]

        print_full_board(position, mask)
        print("Solution is:", solution)
        print("Available Moves:", PlayerX.bitstring_to_avail_moves(position, mask))
        best_move = None
        for move in PlayerX.alphabeta_w_cutoff(
                (position, mask), "X", max_depth=4, max_duration=10):
            best_move = move
        print(best_move)

    exit()

    print_full_board(position, mask)

    print(PlayerX.bitstring_to_avail_moves(position, mask))

    best_move = None
    for move in PlayerX.alphabeta_w_cutoff(
            (position, mask), "X", max_depth=4, max_duration=10):
        best_move = move
    print(best_move)

    for move in PlayerX.alphabeta_w_cutoff(
            (position, mask), "O", max_depth=4, max_duration=10):
        best_move = move
    print(best_move)
    print_full_board(position, position)

    for move in PlayerX.alphabeta_w_cutoff(
            (position ^ mask, position), "O", max_depth=4, max_duration=10):
        best_move = move
    print(best_move)
    print_full_board(position ^ mask, position)

    for move in PlayerX.alphabeta_w_cutoff(
            (position ^ mask, position), "X", max_depth=4, max_duration=10):
        best_move = move
    print(best_move)

    exit()
    # print_board(a)
    # m = a & (a >> 7)

    # # if m > 0 we have two's
    # print_board(m)

    # # if m > 0 we have three's
    # m = m & (m >> 7)
    # print_board(m)

    # if m & (m >> 14):
    # return Truex

    simulate_moves(a, mask)

    exit()

    # print_board(row)
    # print_board(a)
    # print_board(col | mask)
    for i in range(7):
        m = mask & (col << (7 * i))
        print_board(m)
        if m <= 0:
            print("Empty col at", i)
        m = mask & (row << i)
        print_board(m)
        if m <= 0:
            print("Empty row at", i)
