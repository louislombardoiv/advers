import random
import time
import signal

WIDTH = 7
HEIGHT = 7
MAX_DURATION = 10.0
MAX_DEPTH = 3

inf = float('inf')

################################################################################
################################################################################

"""
Previous attempt at forcing the algorithm to exit early
"""


class TimedOutExc(Exception):
    pass


def deadline(timeout, *args):
    def decorate(f):
        def handler(signum, frame):
            raise TimedOutExc()

        def new_f(*args, **kwargs):
            signal.signal(signal.SIGALRM, handler)
            signal.alarm(int(timeout))
            return f(*args, **kwargs)
            signal.alarm(0)

        new_f.__name__ = f.__name__
        return new_f
    return decorate

################################################################################


def print_full_board(bit_str, mask):
    """
    Prints the board from the bitmask, debug func.
    """

    position_str = format(bit_str, '049b')
    opp_str = format(bit_str ^ mask, '049b')

    print("---------")

    for y in range(7):
        print(y, ":", end="")
        for x in reversed(range(7)):
            if '1' == position_str[(x * 7) + y]:
                print('X', end="")
            elif '1' == opp_str[(x * 7) + y]:
                print('O', end="")
            else:
                print("-", end="")
        print("")


def load_player(player):
    # insert file reading code here -- whatever is returned by this function will be saved as lookupX and passed into
    # next_move for use when choosing a move.
    return None


def next_move(board, player, lookup_table):
    """
    Using the alphabeta yield generator find the best possible move
    """
    # Get the starting time
    start_time = time.time()

    # Get the board as a bitmask
    position, mask = get_position_mask_bitmap(
        board.board, 'X' if player == "X" else 'O')

    # Calculate the best move, break if we run out of time
    best_move = None
    while time.time() <= start_time + MAX_DURATION:
        try:
            for move in alphabeta_w_cutoff((position, mask), player, max_depth=MAX_DEPTH):
                best_move = move
        except TimedOutExc as e:
            pass
    return best_move


def bitstring_to_avail_moves(position, mask):
    """
    Check the bitstring for all avail. moves.

    Shuffles the result
    """

    # Get a bit string rep. all the filled spaces
    occupied_places = position | mask
    position_str = format(occupied_places, '049b')

    # Check the bit string at specific points
    actions = [c for c in range(WIDTH) if position_str[HEIGHT * c] == '0']
    if actions:
        random.shuffle(actions)
    return actions


def make_move(position, mask, col):
    """
    Calculate a new bit mask from the current state of the board
    to simulate dropping a piece
    """
    new_position = position ^ mask
    new_mask = mask | (mask + (1 << (col*7)))
    return new_position, new_mask


def get_position_mask_bitmap(board, player):
    """
    Get the board as a bitmask
    """
    position, mask = '', ''
    # Start with right-most column
    for j in range(6, -1, -1):
        # Add 0-bits to sentinel
        mask += '0'
        position += '0'
        # Start with bottom row
        for i in range(0, 6):
            mask += ['0', '1'][board[i][j] != '.']
            position += ['0', '1'][board[i][j] == player]
    return int(position, 2), int(mask, 2)


def connected_four(position):
    """
    Checks for a connection of 4 in any direction
    """
    # Horizontal check
    m = position & (position >> 7)
    if m & (m >> 14):
        return True

    # Diagonal \
    m = position & (position >> 6)
    if m & (m >> 12):
        return True

    # Diagonal /
    m = position & (position >> 8)
    if m & (m >> 16):
        return True

    # Vertical
    m = position & (position >> 1)
    if m & (m >> 2):
        return True

    # Nothing found
    return False


def eval_state(state, current_player, num_moves):
    """
    Evaluates the current state of the board and provides
    a heuristic.
    """
    (position, mask) = state
    connected = connected_four(position)
    opp_connected = connected_four(position ^ mask)

    if opp_connected:
        return WIDTH * HEIGHT + 1 - num_moves / 2

    if connected:
        return -inf

    return 0


def cutoff(state, depth, max_depth, end_time):
    """
    Determine if the game is over ot not.

    (Should exploration stop)
    """

    if time.time() >= end_time:
        return True

    if depth > max_depth:
        return True

    (position, mask) = state
    return connected_four(position) or connected_four(position ^ mask)


def alphabeta_w_cutoff(state, current_player, max_depth=4,
                       max_duration=MAX_DURATION):
    """
    Alpha Beta Pruning implementation.
    """

    def simulate_action(state, action):
        """
        Wrapper so I can use state
        """
        (position, mask) = state
        new_pos, new_mask = make_move(position, mask, action)
        return (new_pos, new_mask)

    def max_value(state, alpha, beta, depth):
        """
        Finds max
        """

        (position, mask) = state  # Unwrap state

        # Get the currently avail. actions
        actions = bitstring_to_avail_moves(position, mask)

        # Check if cutoff has been hit or we have run out of actions
        if cutoff(state, depth, max_depth, end_time) or len(actions) <= 0:

            # Get our current number of moves
            position_as_str = format(position, '049b')
            num_moves = position_as_str.count('1')
            return eval_state(state, current_player, num_moves)

        v = -inf
        for action in actions:
            v = max(v, min_value(simulate_action(
                state, action), alpha, beta, depth + 1))

            if v >= beta:
                return v
            alpha = max(alpha, v)

        return v

    def min_value(state, alpha, beta, depth):
        """
        Finds min
        """

        (position, mask) = state  # Unwrap state

        # Get the currently avail. actions
        actions = bitstring_to_avail_moves(position, mask)

        # Check if cutoff has been hit or we have run out of actions
        if cutoff(state, depth, max_depth, end_time) or len(actions) <= 0:

            # Get our current number of moves
            position_as_str = format(position, '049b')
            num_moves = position_as_str.count('1')
            return eval_state(state, current_player, num_moves)

        v = inf
        for action in actions:
            v = min(v, max_value(simulate_action(
                state, action), alpha, beta, depth + 1))
            if v <= alpha:
                return v
            beta = min(beta, v)
        return v

    def alphabeta_n_step(state, action, best_score, best_action, beta):
        """
        Preform a single step of alpha-beta. Necessary to use this
        as a yield function
        """
        v = min_value(simulate_action(
            state, action), best_score, beta, 1)

        if v > best_score:
            best_score = v
            best_action = action
        return best_action, best_score

    # Calculate the end time
    start_time = time.time()
    end_time = MAX_DURATION + start_time

    # Unwrap the state
    (position, mask) = state
    actions = bitstring_to_avail_moves(position, mask)

    best_score = -inf
    beta = inf
    best_action = None
    for action in actions:
        best_action, best_score = alphabeta_n_step(
            state, action, best_score, best_action, beta)

        # Yield the new best action
        yield best_action
