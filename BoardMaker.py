import os
import keyboard  # using module keyboard
import getch


def print_full_board(bit_str, mask, cursor=(0, 0)):
    position_str = format(bit_str, '049b')
    opp_str = format(bit_str ^ mask, '049b')

    print("---------")

    for y in range(7):
        print(y, ":", end="")
        for x in reversed(range(7)):

            if (x, y) == cursor:
                print("#", end="")
                continue

            if '1' == position_str[(x * 7) + y]:
                print('X', end="")
            elif '1' == opp_str[(x * 7) + y]:
                print('O', end="")
            else:
                print("-", end="")
        print("")
    print("POS:", position_str)
    print("MASK:", format(mask, '049b'))


def clear():
    os.system("clear")


def modify_board_at(bit_str, mask, new_char, x, y):
    position_str = list(format(bit_str, '049b'))
    mask_str = list(format(mask, '049b'))

    if new_char == "O":
        mask_str[(x * 7) + y] = '1'
        position_str[(x * 7) + y] = '0'
    elif new_char == "X":
        mask_str[(x * 7) + y] = '1'
        position_str[(x * 7) + y] = '1'
    elif new_char == "-":
        mask_str[(x * 7) + y] = '0'
        position_str[(x * 7) + y] = '0'

    return int("".join(position_str), 2), int("".join(mask_str), 2), x, y


def manipulate_board(bit_str, mask, key, x, y):

    if key == "s":
        if y < 7:
            y += 1
    elif key == "w":
        if y > 0:
            y -= 1
    elif key == "a":
        if x < 7:
            x += 1
    elif key == "d":
        if x > 0:
            x -= 1
    elif key == "x":
        return modify_board_at(bit_str, mask, 'X', x, y)
    elif key == "o":
        return modify_board_at(bit_str, mask, 'O', x, y)
    elif key == "-":
        return modify_board_at(bit_str, mask, '-', x, y)
    elif key == "q":
        exit()

    return bit_str, mask, x, y


def main():

    bit_str = int(b'0000000000000000000110001101000100000000000000000', 2)
    mask = int(b'0000000000000100000110011111000111100000000000000', 2)

    x = 0
    y = 0

    while True:
        key = None
        while key == None:
            key = chr(ord(getch.getch()))

        bit_str, mask, x, y = manipulate_board(bit_str, mask, key, x, y)
        print_full_board(bit_str, mask, cursor=(x, y))


if __name__ == "__main__":
    main()
